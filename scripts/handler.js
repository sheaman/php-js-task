// Javascript file to handle form and ajax
// Here is a basic skeleton of functions you may choose to work with.  Feel free to write your own if you choose.

// global object declaration
var custObj = {};

var phonenumber = phonenumber ? phonenumber : '9999999999';

phonenumber = phonenumber.replace(/\D/g,'');

// Couple helper functions

// safe console.log
if (typeof console === "undefined") {
	console = {
	  log: function () {
	      return;
	  } 
	};
}

// can use for cache busting on ajax GET requests.
var getRand = function() {
	return Math.floor((Math.random() * 10000) + 1);
}

// call the server to grab anything that has already been saved
var getCustObject = function() {

	// todo: pass phone number to the php script and get a php script to return stored json or a blank obj.
	// retrieve existing json data object, if available.
	// Save to custObj

}


var postCustObject = function() {

	// ajax post the global object to the save_data.php script

}

// basic init to get the saved or base obj and then set all the form data
var init = function () {

	// get customer data based on phone number and set any matching form values based on the json data returned (if any)

	return true;
}

var setFormData = function( leadObjData ) {

	// loop over custObj and set the form values based on any that match id field

}
 

// This loops through all form elements and stores their values on the custObj
function mapFormDataAndSave() {

	// loop over form elements like select, input, textarea, checkbox and store changed values into the custObj

	// now post the customer data
  postCustObject();
  

}

$(function() {

	init();

});