<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>php-js-task</title>
  <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
</head>

<body>

<script>

// TODO: set $phonenumber based on url value and set to a javascript variable to be used in handler.js below
<? echo "var phonenumber = '${phonenumber}';"; ?>

</script>

<form id="script_form">
phone number: <? echo $phonenumber; ?>
first <input type="text" id="first_name" name="first_name"> <br />
last <input type="text" id="last_name" name="last_name"> <br />
address <input type="text" id="address" name="address"> <br />
city <input type="text" id="city" name="city"> <br />
other value <input type="text" id="other" name="other"> <br />

<br />
<b>Creditors</b>
creditorId1 <input type="text" id="creditor_id_1" name="creditor_id_1"> <br />
creditorName2 <input type="text" id="creditor_name_1" name="creditor_name_1"> <br />
creditor monthly payment <input type="text" id="creditor_monthly_1" name="creditor_monthly_1"><br />
creditor interest rate <input type="text" id="creditor_rate_1" name="creditor_rate_1"><br />
creditorBalance1 <input type="text" id="creditor_balance_1" name="creditorBalance_1"> <br />

<br />
<br />

creditorId2 <input type="text" id="creditorId_2" name="creditor_id_2"> <br />
creditorName2 <input type="text" id="creditor_name_2" name="creditor_name_2"> <br />
creditor monthly payment <input type="text" id="creditor_monthly_2" name="creditor_monthly_2"><br />
creditor interest rate <input type="text" id="creditor_rate_2" name="creditor_rate_2"><br />
creditorBalance2 <input type="text" id="creditor_balance_2" name="creditorBalance_2"> <br />

<br />
<!-- only select this if they have selected it before -->
<input type="checkbox" name="hasDebt" id="hasDebt" value="true">

<br />
<select name="typeOfJob" id="typeOfJob">
<option value="">None</option>
<option value="programmer">Programmer</option>
<option value="developer">Developer</option>
<option value="manager">Manager</option>
<option value="freelancer">Freelancer</option>
</select>

</form>

<button onClick="mapFormDataAndSave()"> Go </button>

<!-- handler javascript -->
<script type="text/javascript" src="scripts/handler.js?v=2.5"></script>

</body>
</html>