Task Description:

Read a phone number url value with php, use ajax to call a get data php script.
Store the return data into a javascript object and populate the form.

If no data has been saved yet, just show the basic form.  If it has been saved before, populate the form with the previously saved data.  When the form is modified, modify the json data object and send off to a save_data.php file for storing in a flat text file as a json string.

Requirements:


File Structure:

```
php-js-task
├── README.md
├── index.php
└── scripts
│   ├── handler.js
└── actions
│   ├── save_data.php
│   ├── get_data.php
```

index.php

	(uses jquery script to have access to all jquery functionality)
	
	Sets a javascript variable based on a phone_number url value.
	Using ajax to check get_data.php for any json associated with that phone number.
	Submit button triggers javascript function to scan all form fields and add them to a base javascript object.
	If data already exists on the object it will be overwritten by the changed form field values or added if it is not already part of the object.  Then it sends that data to save_data.php.
	Refreshing the page should repopulate the form with any changed values.

actions

	get_data.php

		Receives a phone number value and looks up ${phone_number}.txt
			If the file exists it returns the json data stored there back to the calling page

	save_data.php

		Receives a phone_number and json body (containing the form values represented as a json object)
		It writes a file called ${phone_number}.txt

scripts

	handler.js

	This will contain all the functions to handle the interactions with the form and the ajax calls.

* I have created a more complex version of this in about 2 hours, that I can share after completing the task.

	